import java.util.Random;

/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class Dice {
    Random mRandom;
    int mRange;

    public Dice(int range){
        mRandom = new Random();
        mRange = range;
    }

    public int roll(){
        return mRandom.nextInt(mRange-1)+1;
    }
}
