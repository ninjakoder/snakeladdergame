/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public interface ITurn {
    public abstract Result playTurn(int oldPosition);
}
