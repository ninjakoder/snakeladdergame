/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class Player{

    String mName;
    int mPos;
    ITurn mPlayTurn;

    public Player(String name){
        mName = name;
    }

    public void setGame(ITurn game){
        mPlayTurn = game;
        mPos = 0;
    }

    public void play(){
        if(mPlayTurn != null){
            System.out.println();
            System.out.println("************");
            System.out.print("|"+mName+" is at "+mPos+"|" + "and rolled dice");
            mPos = mPlayTurn.playTurn(mPos).mNewPosition;
            System.out.print(mName+" is Moving to "+mPos+"|");
        }else{
            System.out.println("No game enrolled");
        }
    }
}
