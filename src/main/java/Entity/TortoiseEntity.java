package Entity;

import java.util.Random;

/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class TortoiseEntity extends Entity {

    public TortoiseEntity(int startPos, int endPos,int boardSize){
        super(ENTITY_TYPE.TORTOISE,startPos,endPos,boardSize);
    }

    @Override
    public int behaviour() {
        return (mStartPos + new Random().nextInt(mBoardSize))%mBoardSize;
    }
}
