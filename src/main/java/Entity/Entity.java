package Entity;

/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class Entity implements IEntity{
    public static enum ENTITY_TYPE{SNAKE,LADDER,TORTOISE};

    int mStartPos;
    int mEndPos;
    static int mBoardSize;
    ENTITY_TYPE mType;

    public Entity(ENTITY_TYPE type,int startPos, int endPos, int boardSize) throws IllegalArgumentException{
        mStartPos = startPos;
        mEndPos = endPos;
        mBoardSize = boardSize;
        mType = type;

        if(!isInValidBounds(startPos,endPos,boardSize)) throw new IllegalArgumentException("Start or End out of board");
        if(!isValidForEntity(startPos,endPos)) throw new IllegalArgumentException("Illegal input for Entity type "+type.name());
    }

    public ENTITY_TYPE getType(){
        return mType;
    }

    public int getStartPosition(){
        return mStartPos;
    }

    public int behaviour(){
     throw new RuntimeException("Behaviour Not Implemented");
    }

    public boolean isInValidBounds(int startPos, int endPos, int mBoardSize){
        return ( (startPos <= mBoardSize) && (endPos <= mBoardSize));
    }

    public boolean isValidForEntity(int startPos, int endPos){
        return true;
    };
}
