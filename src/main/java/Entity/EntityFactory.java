package Entity;

/**
 * Created by Aatmaprem on 27-Nov-15.
 */
public class EntityFactory {
    public static Entity getEntity(Entity.ENTITY_TYPE type, int startPos, int endPos, int boardSize){
        Entity result=null;

        switch (type){
            case SNAKE:
                result = new SnakeEntity(startPos,endPos,boardSize);
                break;

            case LADDER:
                result = new LadderEntity(startPos,endPos,boardSize);
                break;

            case TORTOISE:
                result = new TortoiseEntity(startPos,endPos,boardSize);
                break;
        }

        return result;
    }
}
