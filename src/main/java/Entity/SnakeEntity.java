package Entity;

/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class SnakeEntity extends Entity{

    public SnakeEntity(int startPos,int endPos, int boardSize){
        super(ENTITY_TYPE.SNAKE,startPos,endPos,boardSize);
    }

    @Override
    public int behaviour() {
        return mEndPos;
    }

    public static boolean validate(int startPos, int endPos){
        return (startPos > endPos);
    }

    @Override
    public boolean isValidForEntity(int startPos, int endPos) {
        return mStartPos > mEndPos;
    }
}
