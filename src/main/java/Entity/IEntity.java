package Entity;

/**
 * Created by Aatmaprem on 27-Nov-15.
 */
public interface IEntity {
    public abstract int behaviour();
    public abstract int getStartPosition();
}
