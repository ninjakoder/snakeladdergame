/**
 * Created by Aatmaprem on 27-Nov-15.
 */
public class Result {

    int mNewPosition;
    Game.Game_Status mResultType;

    public Result(Game.Game_Status status, int newPosition){
        this.mNewPosition = newPosition;
        this.mResultType = status;
    }

}
