import Entity.Entity;

import java.util.HashMap;

/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class Board {
    int mBoardSize;
    HashMap<Integer, Entity> mEntityMap;

    public Board(int boardSize){
        mBoardSize = boardSize;
        mEntityMap = new HashMap<Integer, Entity>();
    }

    int playMove(int oldPosition,int move){
        int newPosition = oldPosition + move;
        Entity entity;

        if(newPosition > mBoardSize){
            newPosition = oldPosition;
            System.out.println("New Position out of board, hence staying same postion");
        }
        else if( (entity=mEntityMap.get(newPosition))!=null){
            newPosition = entity.behaviour();
            System.out.println(entity.getType()+" entity found, New position : "+newPosition );
        }

        return newPosition;
    }

    public int getBoardSize(){
        return mBoardSize;
    }

    public void addEntity(Entity entity){
        mEntityMap.put(entity.getStartPosition(),entity);
    }

}
