import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class Game implements ITurn{

    List<Player> mPlayersList;
    Dice mDice;
    Board mBoard;
    int mTurn;
    Game_Status mGameStatus ;

    static enum Game_Status{
        PREPARING,
        RUNNING,
        FINISHED
    }

    public Game(Board board, Dice dice){
        mBoard = board;
        mDice = dice;
        mPlayersList = new ArrayList<Player>();
        mGameStatus = Game_Status.PREPARING;
    }

    public boolean addPlayer(Player player){
        boolean result = false;
        if(mGameStatus == Game_Status.PREPARING){
            player.setGame(this);
            result = mPlayersList.add(player);
            System.out.println("Adding Player "+player.mName);
        }else{
            System.out.println("Game is already "+mGameStatus.name());
        }
        return result;
    }


    public void startGame(){
        mTurn = 0;
        mGameStatus = Game_Status.RUNNING;
    }

    public void finishGame(){
        mGameStatus = Game_Status.FINISHED;
        for(int i = 0 ; i < mPlayersList.size() ; i++){
            System.out.println(mPlayersList.get(i).mName+" has " +
                    ((mTurn==i)?"Won":"Lost") );
        }
    }

    @Override
    public Result playTurn(int oldPosition){

        Result result = null;

        if(mGameStatus == Game_Status.RUNNING){
            int dieRoll = mDice.roll();
            System.out.print("|"+"Die rolled to "+dieRoll+"|");
            int newPosition = mBoard.playMove(oldPosition,dieRoll);

            if(isWin(newPosition)){
                System.out.println("Game is Finished");
                finishGame();
            }

            result = new Result(Game_Status.RUNNING,newPosition);
        }
        else {
            result =  new Result(mGameStatus,oldPosition);
            System.out.println("Cant play. Game is "+ mGameStatus.name());
        }

        return result;
    }

    public boolean isFinished(){
        return mGameStatus== Game_Status.FINISHED;
    }

    public boolean isWin(int position){
        return ( position == mBoard.getBoardSize());
    }

}
