import Entity.Entity;
import Entity.EntityFactory;
import junit.framework.TestCase;

/**
 * Created by Aatmaprem on 25-Nov-15.
 */
public class SnakeLadderTests extends TestCase {

    public void testSimpleGame(){

        Dice dice = new Dice(6);
        Board board = new Board(100);

        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.LADDER,3,50,board.mBoardSize));
        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.LADDER,9,40,board.mBoardSize));
        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.LADDER,20,80,board.mBoardSize));

        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.SNAKE,56,23,board.mBoardSize));
        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.SNAKE,87,24,board.mBoardSize));
        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.SNAKE,45,12,board.mBoardSize));

        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.TORTOISE,23,50,board.mBoardSize));
        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.TORTOISE,12,50,board.mBoardSize));
        board.addEntity(EntityFactory.getEntity(Entity.ENTITY_TYPE.TORTOISE,67,50,board.mBoardSize));

        Player prem = new Player("Prem");
        Player pragya = new Player("Pragya");

        Game game = new Game(board,dice);
        game.addPlayer(prem);
        game.addPlayer(pragya);

        game.startGame();

        try{
            while(!game.isFinished()){
                prem.play();
                Thread.sleep(2000);
                pragya.play();
                Thread.sleep(2000);
            }
        }catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
